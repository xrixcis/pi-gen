#!/bin/bash -e

install -m 644 files/systemd-random-seed.service      "${ROOTFS_DIR}/lib/systemd/system/systemd-random-seed.service"

cat files/bash.logout >> "${ROOTFS_DIR}/etc/bash.bash_logout"

on_chroot << EOF
apt remove -y --purge anacron logrotate rsyslog dphys-swapfile
EOF

on_chroot << EOF
rm -rf /var/lib/dhcp /var/lib/dhcpcd5 /var/run /var/spool /var/lock
ln -s /tmp /var/lib/dhcp
ln -s /tmp /var/lib/dhcpcd5
ln -s /run /var/run
ln -s /tmp /var/spool
ln -s /tmp /var/lock
rm -f /etc/resolv.conf
ln -s /tmp/resolv.conf /etc/resolv.conf
sed -i -e 's/\/etc/\/tmp/' /etc/resolvconf.conf
EOF


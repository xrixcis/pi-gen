#!/bin/bash -e

install -m 644 files/hostname	"${ROOTFS_DIR}/etc/hostname"
install -m 440 files/010_beerberry-nopasswd	"${ROOTFS_DIR}/etc/sudoers.d/010_beerberry-nopasswd"
install -D -m 600 files/authorized_keys	"${ROOTFS_DIR}/home/beerberry/.ssh/authorized_keys"
install -m 644 files/beerberry.service	"${ROOTFS_DIR}/etc/systemd/system/beerberry.service"
install -m 644 files/beerberry-enable.service	"${ROOTFS_DIR}/etc/systemd/system/beerberry-enable.service"
install -m 755 files/beerberry_build	"${ROOTFS_DIR}/usr/bin/beerberry_build"


on_chroot << EOF
chown -R beerberry /home/beerberry

echo "$CONFIG_PASSPHRASE" > /etc/beerberry.passphrase
systemctl enable beerberry-enable
systemctl enable systemd-time-wait-sync
EOF
